import random

# Генерація випадкового цілого числа в діапазоні від 1 до 10
random_number = random.randint(1, 100)

attempts = 5
for i in range(attempts):
    answer = int(input(" Введіть число: "))
    if answer == random_number:
        print("Вітаємо! Ви вгадали правильне число")
        break
    elif answer > random_number:
         print ("Занадто високо") 
    elif answer < random_number:
         print ("Занадто низько")
else:
     print("Вибачте, у вас закінчилися спроби")

